from marshmallow import (
    Schema, pre_load, pre_dump, post_load, validates_schema,
    validates, fields, ValidationError
)

from pumpwoodflask_views import serializers
from pumpwoodflask_views.auth import AuthFactory


######
# List
######
class SerializerPullerJob(serializers.PumpWoodSerializer):
    """Base serialization for PullerJob."""
    model = None
    obj_pk = fields.Integer(allow_none=True)
    created_by_id = fields.Integer(dump_only=True)
    created_at = fields.DateTime(dump_only=True)
    next_run = fields.DateTime(dump_only=True)

    class Meta:
        """Meta."""
        fields = [
            "pk", "model_class", "name", "notes", "inactive", "last_run",
            "next_run", "interval", "interval_offset", "end_point",
            "job_action", "obj_pk", "parameters", "created_at",
            "created_by_id", "queue_set"]
        dump_only = ["created_at", "created_by_id", "next_run", "queue_set"]

    @post_load(pass_many=False)
    def get_created_by_id(self, data):
        """Get logged user for serializer."""
        current_user = AuthFactory.retrieve_authenticated_user()
        data['created_by_id'] = current_user['pk']
        return data


class SerializerPullerQueue(serializers.PumpWoodSerializer):
    """Base serialization for PullerQueue."""
    start_time = fields.DateTime(allow_none=True)
    end_time = fields.DateTime(allow_none=True)
    created_by_id = fields.Integer(dump_only=True)
    created_at = fields.DateTime(dump_only=True)
    model = None

    class Meta:
        """Meta."""
        fields = [
            'pk', 'model_class', "created_at", "start_time", "end_time",
            "status", "results", "created_by_id", "job_id"]
        dump_only = ["created_at", "created_by_id"]

    @post_load(pass_many=False)
    def get_created_by_id(self, data):
        """Get logged user for serializer."""
        current_user = AuthFactory.retrieve_authenticated_user()
        data['created_by_id'] = current_user['pk']
        return data
