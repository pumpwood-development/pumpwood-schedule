# -*- coding: utf-8 -*-
"""Default crawler models."""

# import io
# import pandas
# import simplejson as json
import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta

from sqlalchemy.sql import func
from pumpwood_communication import exceptions
from pumpwoodflask_views.action import action

from sqlalchemy.dialects.postgresql.json import JSONB
from sqlalchemy.ext.declarative import declared_attr

from flask_sqlalchemy import SQLAlchemy

from pumpwoodflask_views.auth import AuthFactory


db = SQLAlchemy()


#################
# Crawler Classes
class SchedulerJob(object):
    """Define default SchedulerJob."""

    """Define Queue Relation."""
    queue_set = None

    """Define one-to-many relation with Puller Queue."""
    name = db.Column(db.String(154), nullable=False, unique=True, index=True)
    notes = db.Column(db.Text(), nullable=True)
    inactive = db.Column(db.Boolean(), server_default="FALSE", default=False)

    # Scheduling parameters
    last_run = db.Column(db.DateTime(
        timezone=False), nullable=False, index=True)
    next_run = db.Column(db.DateTime(
        timezone=False), nullable=True, index=True)
    interval = db.Column(db.String(50), nullable=False, index=True)
    interval_offset = db.Column(db.BigInteger(), nullable=True, index=True)

    # Action parameters
    end_point = db.Column(db.String(50), nullable=False, index=True)
    job_action = db.Column(db.String(50), nullable=False, index=True)
    obj_pk = db.Column(db.BigInteger(), nullable=True, index=True)
    parameters = db.Column(JSONB, server_default='{}')

    created_at = db.Column(db.DateTime(timezone=False), nullable=True,
                           server_default=func.now(), index=True)
    created_by_id = db.Column(db.Integer, nullable=False, index=True)

    """Define name of the SchedulerQueue."""
    scheduler_queue_class = None

    """Set app microservice."""
    microservice = None

    """Set app scheduler rabbitMQ."""
    rabbitmq = None

    def __repr__(self):
        """__repr__."""
        return '<%d:PullerJob %r>' % (self.id or -1, self.name)

    @classmethod
    @action(info="Run schedule.")
    def run_schedule(cls, send_to_rabbitmq: bool=True):
        """Create one or more job queues."""
        auth_header = AuthFactory.get_auth_header()
        now = datetime.datetime.utcnow()
        all_schedules = cls.query.filter_by(inactive=False).all()
        session = cls.query.session

        resp = []
        for s in all_schedules:
            last_run = s.last_run
            next_run = last_run if s.next_run is None else s.next_run

            if next_run <= now:
                while next_run <= now:
                    last_run = next_run
                    if s.interval == "days":
                        next_run = last_run + timedelta(days=s.interval_offset)
                    elif s.interval == "minutes":
                        next_run = last_run + timedelta(
                            minutes=s.interval_offset)
                    elif s.interval == "hours":
                        next_run = last_run + timedelta(
                            hours=s.interval_offset)
                    elif s.interval == "weeks":
                        next_run = last_run + timedelta(
                            weeks=s.interval_offset)
                    elif s.interval == "months":
                        next_run = last_run + relativedelta(
                            months=s.interval_offset)
                    else:
                        raise exceptions.PumpWoodException(
                            "Interval not implemented: {}".format(s.interval))
                s.next_run = next_run
                s.last_run = last_run

                new_queue = cls.microservice.save({
                    "model_class": cls.scheduler_queue_class,
                    "job_id": s.id}, auth_header=auth_header)

                if send_to_rabbitmq:
                    cls.rabbitmq.send(new_queue)
                session.add(s)
                session.commit()
                resp.append(new_queue)
        return resp


QUEUE_STATUS = ("idle", "running", "finished", "error")


class SchedulerQueue(object):
    """Define default PullerQueue."""

    """Define table name for Puller Job."""
    job_id = None
    """Define many-to-one foreign key with Puller Job."""
    job = None
    """Define many-to-one relation with Puller Job."""

    created_at = db.Column(db.DateTime(timezone=False),
                           server_default=func.now(), index=True)
    start_time = db.Column(db.DateTime(timezone=False),
                           nullable=True, index=True)
    end_time = db.Column(db.DateTime(timezone=False),
                         nullable=True, index=True)
    status = db.Column(db.String(20),
                       nullable=False, server_default="idle", index=True,
                       default="idle")
    results = db.Column(JSONB, nullable=False,
                        server_default='{}', default={})
    created_by_id = db.Column(db.Integer, nullable=False, index=True)

    scheduler_job_class = None
    microservice = None
    """PumpWoodMicroService object."""

    @declared_attr
    def __table_args__(cls):
        return (db.CheckConstraint(cls.status.in_(QUEUE_STATUS)), )

    def __repr__(self):
        """__repr__."""
        return '<%d:PullerQueue >' % (self.id or -1, )

    def run_queue(self):
        """Run scheduler queue."""
        self.microservice.login()
        self.status = "running"
        self.start_time = datetime.datetime.utcnow()
        db.session.add(self)
        db.session.commit()

        try:
            results = self.microservice.execute_action(
                model_class=self.job.end_point, action=self.job.job_action,
                pk=self.job.obj_pk, parameters=self.job.parameters)["results"]
        except Exception as e:
            self.status = "error"
            self.results = {"error_msg": str(e)}
            return False

        self.results = results
        self.status = "finished"
        self.end_time = datetime.datetime.utcnow()
        db.session.add(self)
        db.session.commit()
        return True
