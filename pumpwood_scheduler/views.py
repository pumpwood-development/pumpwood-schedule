"""View to scheduler."""
import simplejson as json
from flask import request
from werkzeug.utils import secure_filename
from datetime import datetime
from slugify import slugify

from pumpwoodflask_views.views import PumpWoodFlaskView
from pumpwood_communication import exceptions

from settings.settings import storage_object
from abc import ABC


class SchedulerJobView(PumpWoodFlaskView, ABC):
    """Pumpwood views for Scheduler Job."""

    model_class = None
    list_serializer = None
    retrieve_serializer = None


class SchedulerQueueView(PumpWoodFlaskView, ABC):
    """Pumpwood views for Scheduler Queue."""

    model_class = None
    list_serializer = None
    retrieve_serializer = None
